# Stan-Gallery

I made this project on Xcode 10.0.

#### How to use it:
_Step 1._ Download the project straight away from bitbucket

_Step 2._ Build on Xcode 10.0

_Step 3._ If it doesn't build, try running 'pod install'

_Step 4._ Search for an image you want on the search bar

_Step 5._ Make sure you're connected to internet to get the list of image posts

_Step 6._ Use the switch button on the top right to toggle the image filter


__Platform:__ iPhone, iPad

__Libraries used:__ Swift 4.2, XCode 10.0, Alamofire, SDWebImage

__Architecture:__ 
It uses MVP pattern where ViewController class is the View, Presenter class is the Presenter, Property Unit struct is the Model, In the data layer, it communicates with API client to retrieve some data from the server.