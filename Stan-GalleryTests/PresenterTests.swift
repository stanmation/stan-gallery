//
//  PresnterTests.swift
//  Stan-GalleryTests
//
//  Created by Stanley Darmawan on 10/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import XCTest
@testable import Stan_Gallery
import Alamofire

class PresenterTests: XCTestCase {
  var sut: Presenter!
  var apiClient: APIClientMock!
  var viewController: ViewControllerMock!
  
  override func setUp() {
    super.setUp()

    apiClient = APIClientMock()
    viewController = ViewControllerMock()
    sut = Presenter(display: viewController, apiClient: apiClient)
  }
  
  override func tearDown() {
    apiClient = nil
    viewController = nil
    sut = nil
    
    super.tearDown()
  }
  
  func test_GetImagesWithSuccessResponse_ReloadsUI() {
    let result = SuccessResultStub().result
    
    apiClient.result = Result<ImagePostResponse>.success(result!)
    sut.getImages(withQuery: "Cat")
    
    XCTAssertTrue(viewController.didReload)
  }
  
  func test_GetImagesWithSuccessResponse_SendsDataToCellItems() {
    let result = SuccessResultStub().result

    apiClient.result = Result<ImagePostResponse>.success(result!)
    sut.getImages(withQuery: "Cat")
    
    let cellItem = sut.imagePostCellItems().first
    XCTAssertEqual(cellItem?.title, "Life of a cat")
    XCTAssertEqual(cellItem?.date, Date(timeIntervalSince1970: 1549573278))
    XCTAssertEqual(cellItem?.imageURLString, "https://image.com")
    XCTAssertEqual(cellItem?.numberOfAdditionalImages, 0)
  }
  
  func test_GetImagesWithSuccessResponseAndFilterOn_DoesntDisplaysCellWithOddNumberTotalScoring() {
    sut.isFilterOn = true
    
    let result = SuccessResultStubWithNumberOddTotalPoints().result
    
    apiClient.result = Result<ImagePostResponse>.success(result!)
    sut.getImages(withQuery: "Cat")
    
    let cellItem = sut.imagePostCellItems().first
    XCTAssertNil(cellItem)
  }
  
  func test_GetMultipleImagesWithSuccessResponse_OrdersCellsInReverseChronologicalOrder() {
    let imagePost1 = SuccessResultStub().imagePost
    let imagePost2 = SuccessResultStubWithNumberOddTotalPoints().imagePost
    let result = ImagePostResponse(imagePosts: [imagePost2!, imagePost1!])
    
    apiClient.result = Result<ImagePostResponse>.success(result)
    sut.getImages(withQuery: "Cat")
    
    let cellItems = sut.imagePostCellItems()
    XCTAssertEqual(cellItems[0].date, Date(timeIntervalSince1970: 1549573279))
    XCTAssertEqual(cellItems[1].date, Date(timeIntervalSince1970: 1549573278))
  }
  
  func test_GetMultipleImagesWithSuccessResponse_GivesCellItemNumberOfAdditionalImages() {
    let image1 = SuccessResultStub().image
    let image2 = SuccessResultStubWithNumberOddTotalPoints().image
    
    let imagePost = ImagePost(title: "",
                              dateTimeInInterval: 7737737,
                              images: [image1!, image2!],
                              points: 3,
                              score: 2,
                              topicID: 1)
    let result = ImagePostResponse(imagePosts: [imagePost])
    
    apiClient.result = Result<ImagePostResponse>.success(result)
    sut.getImages(withQuery: "Cat")
    
    let cellItems = sut.imagePostCellItems()
    XCTAssertEqual(cellItems.first?.numberOfAdditionalImages, 1)
  }
  
  func test_GettingImagesWithErrorResponse_DisplaysErrorAlert() {
    let error = NSError()
    apiClient.result = Result<ImagePostResponse>.failure(error)
    sut.getImages(withQuery: "Cat")
    
    XCTAssertTrue(viewController.didShowErrorAlert)
  }
  
  func test_GettingImagesWithEmptyResult_DisplaysEmptyResultAlert() {
    let response = ImagePostResponse(imagePosts: [])
    apiClient.result = Result<ImagePostResponse>.success(response)
    sut.getImages(withQuery: "afafafadf")
    
    XCTAssertTrue(viewController.didShowEmptyResultAlert)
  }
}

class SuccessResultStub {
  var result: ImagePostResponse!
  var imagePost: ImagePost!
  var image: Image!
  
  init() {
    image = Image(link: "https://image.com", type: .png)
    imagePost = ImagePost(title: "Life of a cat",
                          dateTimeInInterval: 1549573278, // 2019-02-07 21:01:18 +0000
                          images: [image],
                          points: 3,
                          score: 5,
                          topicID: 4)
    result = ImagePostResponse(imagePosts: [imagePost])
  }
}

class SuccessResultStubWithNumberOddTotalPoints {
  var result: ImagePostResponse!
  var imagePost: ImagePost!
  var image: Image!

  init() {
    image = Image(link: "https://image.com", type: .png)
    imagePost = ImagePost(title: "Life of a dog",
                          dateTimeInInterval: 1549573279, // 2019-02-07 21:01:19 +0000
                          images: [image],
                          points: 3,
                          score: 5,
                          topicID: 5)
    result = ImagePostResponse(imagePosts: [imagePost])
  }
}

class APIClientMock: APIClient {
  var result: Result<ImagePostResponse>!
  
  override init() {}
  
  override func getImages(query: String, completionHandler: @escaping (Result<ImagePostResponse>) -> Void) {
    completionHandler(result)
  }
}

class ViewControllerMock: PresenterDisplay {
  var didShowLoadingIndicator = false
  var didHideLoadingIndicator = false
  var didReload = false
  var didShowErrorAlert = false
  var didShowEmptyResultAlert = false
  
  func showLoadingIndicator() {
    didShowLoadingIndicator = true
  }
  
  func hideLoadingIndicator() {
    didHideLoadingIndicator = true
  }
  
  func reload() {
    didReload = true
  }
  
  func showErrorAlert() {
    didShowErrorAlert = true
  }
  
  func showEmptyResultAlert() {
    didShowEmptyResultAlert = true
  }
}

