//
//  ViewController.swift
//  Stan-Gallery
//
//  Created by Stanley Darmawan on 9/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var filterSwitch: UISwitch!
  @IBOutlet var progressIndicator: UIActivityIndicatorView!
  
  lazy var presenter: Presenter = Presenter(display: self)
  let searchController = UISearchController(searchResultsController: nil)

  override func viewDidLoad() {
    super.viewDidLoad()
    
    prepareSearchController()
  }
  
  private func prepareSearchController() {
    searchController.searchBar.placeholder = "Search Images"
    searchController.searchResultsController?.view.isHidden = false
    navigationItem.searchController = searchController
    navigationItem.hidesSearchBarWhenScrolling = false
    definesPresentationContext = true

    searchController.searchBar.delegate = self
  }
  
  @IBAction func toggleFilterSwitch(_ sender: UISwitch) {
    presenter.isFilterOn = sender.isOn
  }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
  // MARK: - UITableView DataSource / Delegate

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter.imagePostCellItems().count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let identifier = String(describing: ImagePostTableViewCell.self)
    if let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ImagePostTableViewCell {
      let imagePostCellItem = presenter.imagePostCellItems()[indexPath.row]
      cell.configure(with: imagePostCellItem)
      return cell
    }
    
    fatalError("Error in Creating UITableViewCell")
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

extension ViewController: UISearchBarDelegate {
  // MARK: - UISearchBar Delegate
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    if let text = searchBar.text {
      presenter.getImages(withQuery: text)
    }
    searchController.isActive = false
  }
}

extension ViewController: PresenterDisplay {
  // MARK: - PresenterDisplay
  
  func showLoadingIndicator() {
    progressIndicator.startAnimating()
  }
  
  func hideLoadingIndicator() {
    progressIndicator.stopAnimating()
  }
  
  func reload() {
    tableView.reloadData()
  }
  
  func showErrorAlert() {
    let alertController = UIAlertController(title: "Error", message: "An error occured", preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(action)
    present(alertController, animated: true, completion: nil)
  }
  
  func showEmptyResultAlert() {
    let alertController = UIAlertController(title: "No Result", message: "No Result from your search",
                                            preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(action)
    present(alertController, animated: true, completion: nil)
  }
}
