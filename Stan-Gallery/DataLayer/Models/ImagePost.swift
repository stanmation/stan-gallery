//
//  Post.swift
//  Stan-Gallery
//
//  Created by Stanley Darmawan on 9/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import Foundation

struct ImagePostResponse: Decodable {
  let imagePosts: [ImagePost]
  
  enum CodingKeys: String, CodingKey {
    case imagePosts = "data"
  }
}

struct ImagePost: Decodable {
  let title: String
  let dateTimeInInterval: TimeInterval
  let images: [Image]?
  let points: Int
  let score: Int
  let topicID: Int
  
  enum CodingKeys: String, CodingKey {
    case title
    case dateTimeInInterval = "datetime"
    case images
    case points
    case score
    case topicID = "topic_id"
  }
  
  var dateTimeConverted: Date {
    return Date(timeIntervalSince1970: dateTimeInInterval)
  }
}

struct Image: Decodable {
  enum ImageType: String, Codable {
    case jpeg = "image/jpeg"
    case png = "image/png"
    case gif = "image/gif"
    case mp4 = "video/mp4"
  }
  let link: String
  let type: ImageType
}

