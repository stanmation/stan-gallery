//
//  Presenter.swift
//  Stan-Gallery
//
//  Created by Stanley Darmawan on 10/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import UIKit
import Alamofire

protocol PresenterDisplay: class {
  func showLoadingIndicator()
  func hideLoadingIndicator()
  func reload()
  func showErrorAlert()
  func showEmptyResultAlert()
}

final class Presenter {
  private var imagePosts = [ImagePost]()
  
  var isFilterOn = false {
    didSet {
      display.reload()
    }
  }
  
  unowned let display: PresenterDisplay
  let apiClient: APIClient
  
  init(display: PresenterDisplay,
       apiClient: APIClient = APIClient()) {
    self.display = display
    self.apiClient = apiClient
  }
  
  func getImages(withQuery query: String) {
    display.showLoadingIndicator()
    
    apiClient.getImages(query: query) { [weak self] result in
      self?.display.hideLoadingIndicator()
      switch result {
      case .success(let imagePostResponse):
        if imagePostResponse.imagePosts.isEmpty {
          self?.display.showEmptyResultAlert()
        }
        self?.imagePosts = imagePostResponse.imagePosts
      case .failure(_):
        self?.display.showErrorAlert()
      }
      self?.display.reload()
    }
  }
  
  func imagePostCellItems() -> [ImagePostCellItem] {
    let modifiedImagePosts = imagePosts
      .sorted(by: { $0.dateTimeInInterval > $1.dateTimeInInterval })
      .filter {
        if self.isFilterOn {
          return ($0.score + $0.topicID + $0.points) % 2 == 0
        }
        return true
      }
    
    return modifiedImagePosts.map {
      generateCellItem(imagePost: $0)
    }
  }
  
  private func generateCellItem(imagePost: ImagePost) -> ImagePostCellItem {
    if let images = imagePost.images {
      let isUsableURL = (imagePost.images?.first?.type != .mp4
        && imagePost.images?.first?.type != .gif)
      let numberOfAdditionalImages = images.endIndex > 1 ? images.count - 1 : 0
      let imageURL = isUsableURL ? imagePost.images?.first?.link : nil

      return ImagePostCellItem(title: imagePost.title,
                               date: imagePost.dateTimeConverted,
                               numberOfAdditionalImages: numberOfAdditionalImages,
                               imageURLString: isUsableURL ? imageURL : nil)
    } else {
      
      return ImagePostCellItem(title: imagePost.title,
                               date: imagePost.dateTimeConverted,
                               numberOfAdditionalImages: 0,
                               imageURLString: nil)
    }
  }
}
