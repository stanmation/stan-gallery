//
//  ImagePostTableViewCell.swift
//  Stan-Gallery
//
//  Created by Stanley Darmawan on 10/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import UIKit
import SDWebImage

class ImagePostTableViewCell: UITableViewCell {
  
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var dateLabel: UILabel!
  @IBOutlet var imgView: UIImageView!
  @IBOutlet var imgCountLabel: UILabel!
  @IBOutlet var imgViewHeightConstraint: NSLayoutConstraint!
  
  override func prepareForReuse() {
    imgView.sd_cancelCurrentImageLoad()
    imgViewHeightConstraint.constant = 116
    imgCountLabel.isHidden = false
  }
  
  func configure(with cellItem: ImagePostCellItem) {
    titleLabel.text = cellItem.title
    dateLabel.text = DateFormat.dateMonthYearHourMinute.convertToString(from: cellItem.date)
    
    guard let imageURLString = cellItem.imageURLString else {
      imgViewHeightConstraint.constant = 0
      imgCountLabel.isHidden = true
      return
    }
    
    imgView.sd_setImage(with: URL(string: imageURLString),
                        placeholderImage: UIImage(named: "noImage"),
                        completed: nil)
    
    if cellItem.numberOfAdditionalImages > 1 {
      imgCountLabel.text = "Number of additional images: \(String(cellItem.numberOfAdditionalImages))"
    } else {
      imgCountLabel.isHidden = true
    }
  }
}
