//
//  ImagePostCellItem.swift
//  Stan-Gallery
//
//  Created by Stanley Darmawan on 10/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import Foundation

struct ImagePostCellItem {
  let title: String
  let date: Date
  let numberOfAdditionalImages: Int
  let imageURLString: String?
}
