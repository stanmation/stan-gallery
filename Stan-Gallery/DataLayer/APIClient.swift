//
//  APIClient.swift
//  Stan-Gallery
//
//  Created by Stanley Darmawan on 9/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import Alamofire

class APIClient {  
  private let headers = ["Authorization": "Client-ID 3dc9ca4c70ba43f"]
  private let serviceBaseURL = "https://api.imgur.com"
  
  func getImages(query: String, completionHandler: @escaping (_ response: Result<ImagePostResponse>) -> Void) {
    let path = "/3/gallery/search/top/week"
    let params = ["q": query]
    Alamofire.request(serviceBaseURL+path, method: .get, parameters: params, headers: headers).responseDecodable { (response: DataResponse<ImagePostResponse>) in
      completionHandler(response.result)
    }
  }
}

