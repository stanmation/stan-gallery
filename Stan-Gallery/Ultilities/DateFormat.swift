//
//  DateFormat.swift
//  Stan-Gallery
//
//  Created by Stanley Darmawan on 10/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import Foundation

// Costs in performance, so need to create one instance of DateFormatter only
private let dateFormatter = DateFormatter()

enum DateFormat: String {
  case dateMonthYearHourMinute = "dd/MM/YYYY h:mm a"
  
  func convertToString(from date: Date) -> String {
    dateFormatter.dateFormat = self.rawValue
    return dateFormatter.string(from: date)
  }
}
